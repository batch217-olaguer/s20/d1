console.log("Wassup World?");

// [SECTION] While Loop
// "Iteration" is the term given to the repetition of statements

// SYNTAX -> while(expression/condition){statement}

/*let count = 5;

while(count !== 0){
	console.log("While: " + count);
	count--; // (--) --> decrementation, decreasing the value by 1.
}

// [SECTION] Do-While Loop
// do{statement}
// while(expression/condition)

let number = Number(prompt("Give me a number."));

do{
	// The current value of the number is printed out.
	console.log("Do While: " + number);

	// Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater.
	number += 1;
	// number = number + 1;
} while(number < 10);

//
for (let count = 0; count <= 20; count++){
	console.log(count);
}

for (let count = 0; count <= 20; count++){
	console.log(count);
}*/

/*let myString = "Alex";
console.log(myString.length);

console.log(myString[0]); 
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

// console.log(myString[-1]);

for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}*/

let myName = "olaguer";

for(let i = 0; i < myName.length; i++){
	// console.log(myName[i].toLowerCase());

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		){
		console.log(3)
	} else {
		console.log(myName[i]);
	}
}

for (let count = 0; count <= 20; count++){

	if (count % 2 === 0){
		// Tells the code to continue to the next iteration of the loop
		// This ignores all statements located after the continue statement;
		continue;
	}
	console.log("Continue and break: " + count);

	if (count > 10){
		break;
	}
}

let name = "Alexandro";

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration.");
		continue;
	}

	if (name[i] == "d"){
		break;
	}
}